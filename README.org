#+TITLE:   buildah
#+DATE:    February 11, 2020
#+STARTUP: inlineimages nofold

* Table of Contents :TOC_3:noexport:
- [[#description][Description]]
  - [[#maintainers][Maintainers]]
- [[#configuration][Configuration]]
  - [[#flags][Flags]]
  - [[#environment-variables][Environment Variables]]

* Description
Build containers with buildah or podman in a rootless environment.

#+NAME: build container in container
#+BEGIN_SRC shell
podman run -it --rm \    # Privileged is needed to allow buildah some action inside the container
-v $PWD:/work buildah \  # Mount your local dir into the container
-f /work/Dockerfile \    # set the Dockerfile to build from
--destination cocainefarm/buildah:latest \  # Define one or more destinations to push your images to.
--destination quay.io/cocainefarm/buildah:latest
#+END_SRC

** Maintainers
+ [[https://github.com/maxaudron][@maxaudron]] (Author)

* Configuration
The configuration and usage is mostly the same as buildah, but i added some
utility for building in a CI context.

** Flags
+ ~--destination~ allows you to specify multiple destinations to push the image
  to when done building without having to manually tag and push them.

** Environment Variables
Useful when running in a CI environment like GitLab CI where you can define a
single template and only have to set a few environment variables.

+ ~BUILD_ARGS~ is an Array you can use to pass values to the container with ~--build-args~.
+ ~DESTINATION~ is an Array you can use to pass destinations the image should be
  pushed to.
