#!/bin/sh

# Check if an argument was provided to the given option;
check_arg_exist() {
  if [ -z "$2" ] || [ "$2" = "-"* ]; then
    echo "ERROR: argument $1 requires an argument!"
    exit 1
  fi
}

while [[ "$1" != "" ]]; do
  case "$1" in
    -d | --destination)
      check_arg_exist "$1" "$2"
      DESTINATION="$DESTINATION $2"
      shift 2
      ;;
    *)
      INPUT="$INPUT ${1}"
      shift
      ;;
  esac
done

DESTINATION="${DESTINATION## }"

build_build_args() {
  for arg in ${BUILD_ARGS}; do
    printf ' --build-arg %s' "$arg"
  done
}

build_storage_settings() {
  if [ -n "$BUILDAH_ROOT" ]; then
    printf ' --root %s' "$BUILDAH_ROOT"
  fi

  if [ -n "$BUILDAH_RUNROOT" ]; then
    printf ' --runroot %s' "$BUILDAH_RUNROOT"
  fi
}

echo -e "\n-------------------------------------------------------------------------"
echo "Building image: ${DESTINATION%% *}"
echo "-------------------------------------------------------------------------"
if [ -n "$DRY_RUN" ]; then
  echo "buildah $(build_storage_settings) bud $(build_build_args) -t \"${DESTINATION%% *}\" $INPUT"
else
  buildah $(build_storage_settings) bud$(build_build_args) -t "${DESTINATION%% *}" $INPUT
fi

for dest in ${DESTINATION}; do
  echo -e "\n-------------------------------------------------------------------------"
  echo "Pushing image: ${dest}"
  echo "-------------------------------------------------------------------------"
  if [ -n "$DRY_RUN" ]; then
    echo "buildah $(build_storage_settings) tag \"${DESTINATION%% *}\" \"${dest}\""
    echo "buildah $(build_storage_settings) push \"${dest}\""
  else
    buildah $(build_storage_settings) tag "${DESTINATION%% *}" "${dest}"
    buildah $(build_storage_settings) push "${dest}"
  fi
done
